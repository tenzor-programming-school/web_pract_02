
function task21(arr) {

    const groupByed = {} // массив под сгруппированные данные

    // группируем массивы с данными по годам и месяцам, подсчитываем кол-во операций
    arr.forEach(operation => {
        const key = `${operation.year}-${operation.month}`;
        groupByed[key] = (groupByed[key] || 0) + 1;
    })

    // создаём обновленный сгруппированный массив объектов:
    // разъединяем ключ, вводим переменную opsCount в массив
    const sorted = Object.keys(groupByed).map(key => {
        const [year, month] = key.split('-').map(Number);
        return {year, month, opsCount: groupByed[key]}
    }).sort(function (a, b) { return b.opsCount - a.opsCount })

    arr = sorted.slice(0, 3)
    return arr;
}

function task22(year, month, arr) {
    let replenishments = 0; // сумма всех пополнений
    let withdrawals = 0; // сумма всех снятий наличных
    let payments = 0; // сумма всех трат

    // определяем крайний день в месяце используя передаваемые месяц и год
    const date = new Date(year, month, 1);
    date.setDate(date.getDate() - 1);

    arr.forEach(operation => {
        if (operation.year === year && operation.month === month) {
            switch (operation.type) {
                case 'replenishment':
                    replenishments += operation.amount;
                    break;
                case 'withdrawal':
                    withdrawals += operation.amount;
                    break;
                case 'payment':
                    payments += operation.amount;
                    break;
                default:
                    break;
            }
        }
    });
    let monthBalance = 0; // по условию - изначально равен нулю
    monthBalance = replenishments - (withdrawals + payments);
    const monthWithdrawal = withdrawals;

    let withdrawalRate = 0;
    if (withdrawals > 0) { withdrawalRate = ((withdrawals + payments) / replenishments).toFixed(4); }

    let rank = '.'

    if (withdrawalRate < 0.15) {
        rank = 'Золотой';
    }
    else if (withdrawalRate < 0.3) {
        rank = 'Серебряный';
    }
    else {
        rank = 'Бронзовый';
    }
    return {
        date: `${year}-${String(month).padStart(2, '0')}-${date.getDate()}`,
        monthBalance,
        monthWithdrawal,
        withdrawalRate,
        rank
    }
}

function task23(arr) {
    
    const result = [];
    let totalBalance = 0;
    
    const startYear = Math.min(...arr.map(op => op.year));
    const endYear = Math.max(...arr.map(op => op.year));

    for (let year = startYear; year <= endYear; year++) {
        for (let month = 1; month <= 12; month++){
            if (arr.find(op => op.year === year && op.month === month) !== undefined) {
                const predTaskConsts = task22(year, month, arr);
                totalBalance += predTaskConsts.monthBalance;
                result.push({ ...predTaskConsts, totalBalance })
                }
        }
    }
    return result;
}